package pondy.coopon.org.pprgjipmer.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.adapters.VideoPlaylistAdapter;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Video;
import pondy.coopon.org.pprgjipmer.rest.ApiClient;
import pondy.coopon.org.pprgjipmer.rest.PPRGInterface;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContentsViewFragment} factory method to
 * create an instance of this fragment.
 */
public class ContentsViewFragment extends Fragment {

    // Views
    @BindView(R.id.recycler_view_videos)
    RecyclerView recyclerView;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.smooth_progress_bar)
    SmoothProgressBar progressBar;
    private Mother masterPatient = new Mother();
    private List<Video> videoList = new ArrayList<>();
    private VideoPlaylistAdapter adapter;


    public ContentsViewFragment() {
        // Required empty public constructor
    }

    public ContentsViewFragment(Mother masterPatient) {
        this.masterPatient = masterPatient;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contents_view, container, false);


        ButterKnife.bind(this, rootView);
        adapter = new VideoPlaylistAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        /*
         * Search View
         */
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getString(R.string.hint_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                List<Video> filterList = new ArrayList<>();
                for (Video video : videoList) {
                    String rec = video.getTitle() + " " + video.getDescription();
                    rec = rec.replace("null", "").toLowerCase();
                    if (rec.contains(newText.toLowerCase())) {
                        filterList.add(video);
                    }
                }

                adapter.setVideos(filterList);
                adapter.notifyDataSetChanged();

                return false;
            }
        });

        getVideoPlayList();

        return rootView;
    }

    private void getVideoPlayList() {

        String data = GeneralUtils.getAssetVideoJsonData(Objects.requireNonNull(getContext()));
        Type type = new TypeToken<List<Video>>(){}.getType();
        List<Video> videos = new Gson().fromJson(data, type);
        assert videos != null;
        videoList.addAll(videos);
        adapter.setVideos(videoList);
        adapter.notifyDataSetChanged();


       /* Retrofit retrofit = ApiClient.getRetrofit();
        PPRGInterface pprgInterface = retrofit.create(PPRGInterface.class);



        pprgInterface.videoPlaylist().enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(@NonNull Call<List<Video>> call, @NonNull Response<List<Video>> response) {

                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {
                    videoList.addAll(response.body());
                    adapter.setVideos(videoList);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Video>> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                ToastUtils.showError(getContext(), getString(R.string.msg_check_network_connection));
            }
        });*/


    }

}
