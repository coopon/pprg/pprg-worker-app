package pondy.coopon.org.pprgjipmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Subject {

    @SerializedName("ask")
    @Expose
    private List<Task> ask = null;
    @SerializedName("examine")
    @Expose
    private List<Task> examine = null;
    @SerializedName("counsel")
    @Expose
    private List<Task> counsel = null;

    public List<Task> getAsk() {
        return ask;
    }

    public void setAsk(List<Task> ask) {
        this.ask = ask;
    }

    public List<Task> getExamine() {
        return examine;
    }

    public void setExamine(List<Task> examine) {
        this.examine = examine;
    }

    public List<Task> getCounsel() {
        return counsel;
    }

    public void setCounsel(List<Task> counsel) {
        this.counsel = counsel;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "ask=" + ask +
                ", examine=" + examine +
                ", counsel=" + counsel +
                '}';
    }
}