package pondy.coopon.org.pprgjipmer.interfaces;

import android.location.Location;

public interface MapInterface {
    public void patientLocation(Location location, String patientID, String patientName);
}
