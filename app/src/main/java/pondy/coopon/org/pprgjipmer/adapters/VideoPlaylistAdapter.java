package pondy.coopon.org.pprgjipmer.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.VideoPlayerActivity;
import pondy.coopon.org.pprgjipmer.model.Video;

public class VideoPlaylistAdapter extends RecyclerView.Adapter<VideoPlaylistAdapter.ViewHolder> {

    private List<Video> mList = new ArrayList<>();
    private Context mContext;

    public VideoPlaylistAdapter(Context context) {
        this.mContext = context;
    }

    public void setVideos(List<Video> videoList) {
        this.mList = videoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_videos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Video video = mList.get(holder.getAdapterPosition());

        Glide.with(mContext)
                .load("file:///android_asset/videos/" + video.getThumbnailUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.edit_text_custom_bg)
                .into(holder.imgThumbnail);

        holder.txtTitle.setText(video.getTitle());
        holder.txtDescription.setText(video.getDescription());

        String duration = "";

        if(video.getDuration() != null){

            if(video.getDuration().contains(":")) {
                int min = Integer.parseInt(video.getDuration().split(":")[0]);
                int sec = Integer.parseInt(video.getDuration().split(":")[1]);
                if(min > 0)
                    duration = min + "m ";
                duration = duration + sec + "secs";
            }else
                duration = video.getDuration();

        }

        String info =  duration + " ~ " + (video.getSize() != null ? video.getSize() : "");
        holder.txtInfo.setText(info);

        holder.itemView.setOnClickListener(v -> openVideoPlayerActivity(video.getVideoUrl()));

    }

    private void openVideoPlayerActivity(String videoUrl) {
        Intent outgoing = new Intent(mContext, VideoPlayerActivity.class);
        outgoing.putExtra("Video", videoUrl);
        mContext.startActivity(outgoing);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_thumbnail)
        ImageView imgThumbnail;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_description)
        TextView txtDescription;
        @BindView(R.id.txt_info)
        TextView txtInfo;
        @BindView(R.id.btn_download)
        ImageButton btnDownload;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
