package pondy.coopon.org.pprgjipmer.interfaces;

import java.util.List;

import pondy.coopon.org.pprgjipmer.model.Subject;
import pondy.coopon.org.pprgjipmer.model.Task;

public interface SubjectUpdateListener {
    public void onSubjectUpdate(Subject subject);
}
