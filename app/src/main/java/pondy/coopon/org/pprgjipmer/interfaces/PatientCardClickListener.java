package pondy.coopon.org.pprgjipmer.interfaces;

import pondy.coopon.org.pprgjipmer.model.Mother;

public interface PatientCardClickListener {
    public void onClick(Mother patient);
}
