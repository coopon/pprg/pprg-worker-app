package pondy.coopon.org.pprgjipmer.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.ListenerToken;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryChange;
import com.couchbase.lite.QueryChangeListener;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.PatientTabActivity;
import pondy.coopon.org.pprgjipmer.adapters.PatientRecordAdapter;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.interfaces.PatientCardClickListener;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.constant.PatientRecordConst;


public class ScheduleFragment extends Fragment {

    private static final String TAG = "ScheduleFragment";

    // Helper
    private SharedPref sharedPref;
    private DatabaseManager dbMgr;

    // Views
    private View rootView;
    @BindView(R.id.txt_main_month_year)
    TextView txtMainMonYear;
    @BindView(R.id.txt_go_to_today)
    TextView txtGoToToday;
    @BindView(R.id.txt_no_of_visits)
    TextView txtNoOfVisits;
    @BindView(R.id.txt_no_visits_planned)
    TextView txtNoVisitsPlanned;
    @BindView(R.id.recycler_view_schedule)
    RecyclerView recyclerView;
    @BindView(R.id.compact_calendar_view)
    CompactCalendarView compactCalendarView;
    @BindView(R.id.btn_calender_expand)
    ImageView btnCalExpand;

    // Variables
    private PatientRecordAdapter adapter;
    private boolean ifExpand = true;
    private Date selectedDate;
    private Query query;
    private ListenerToken queryListenerToken;
    List<Mother> patientList = new ArrayList<>();

    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: called");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: called");

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_schedule, container, false);

        ButterKnife.bind(this, rootView);
        dbMgr = DatabaseManager.getSharedInstance(getContext());
        sharedPref = new SharedPref(getContext());

        // Load all records from db
        loadAllRecords();

        PatientCardClickListener listener = patient -> {
            Intent outgoing = new Intent(getContext(), PatientTabActivity.class);
            outgoing.putExtra("PATIENT_ID", patient.getMainId());
            startActivity(outgoing);
        };

        adapter = new PatientRecordAdapter(getContext(), PatientRecordConst.SCHEDULE, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        calenderConfig();

        return rootView;
    }


    private void calenderConfig() {

        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class
        compactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        //compactCalendarView.setIsRtl(false);
        //compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.dark_red));
        compactCalendarView.displayOtherMonthDays(false);
        //compactCalendarView.invalidate();

        /*
         *  Set up listeners.
         *  Date click and Month Change
         */
        // define a listener to receive callbacks when certain events happen.
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);
                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
                setDateOnCalender(dateClicked);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                Calendar cal = Calendar.getInstance();
                cal.setTime(firstDayOfNewMonth);
                markScheduleDatesInCalendar(cal); // Mark schedule dates to calender
                setDateOnCalender(firstDayOfNewMonth);
            }
        });


        /*
         * Expand and Collapse control
         */
        btnCalExpand.setOnClickListener(v -> {
            if (ifExpand)
                compactCalendarView.hideCalendar();
            else
                compactCalendarView.showCalendar();
            btnCalExpand.setImageResource(ifExpand ? R.drawable.ic_cal_arrow_down : R.drawable.ic_cal_arrow_up);
            ifExpand = !ifExpand;
            new Handler().postDelayed(() -> txtMainMonYear.setText(DateTimeUtils.convertDateToString(selectedDate, ifExpand ? DateTimeUtils.dateFormat_MMM_yyyy : DateTimeUtils.dateFormat_dd_MMM_yyyy)), 700);
        });

        /*
         * Go to Today option
         */
        txtGoToToday.setOnClickListener(v -> setDateOnCalender(new Date()));


        /*
         * Default current date
         */
        setDateOnCalender(new Date());
        compactCalendarView.setCurrentDate(new Date());
        markScheduleDatesInCalendar(Calendar.getInstance());
        ifExpand = true;
    }

    private void setDateOnCalender(Date dateClicked) {
        selectedDate = dateClicked;
        compactCalendarView.setCurrentDate(selectedDate);
        txtMainMonYear.setText(DateTimeUtils.convertDateToString(selectedDate, ifExpand ? DateTimeUtils.dateFormat_MMM_yyyy : DateTimeUtils.dateFormat_dd_MMM_yyyy));
        loadScheduleRecordsOnSelectedDate(DateTimeUtils.convertDateToString(selectedDate, DateTimeUtils.dateFormat_yyyy_MM_dd));
    }

    private QueryChangeListener queryChangeListener = new QueryChangeListener() {
        @Override
        public void changed(QueryChange change) {
            ResultSet resultRows = change.getResults();

            patientList.clear();
            for (Result row : resultRows)
                patientList.add(dbMgr.buildPatient(row));


            // After change data to refresh
            loadScheduleRecordsOnSelectedDate(DateTimeUtils.convertDateToString(selectedDate, DateTimeUtils.dateFormat_yyyy_MM_dd));
            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedDate);
            markScheduleDatesInCalendar(cal); // Mark schedule dates to calender
        }
    };

    // Init all patient values in list
    private void loadAllRecords() {
        try {
            query = dbMgr.getAllPatients();
            queryListenerToken = query.addChangeListener(queryChangeListener);
            query.execute();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    private void markScheduleDatesInCalendar(Calendar calendar) {
        String strYyyy_MM = DateTimeUtils.convertCalenderToStringDateTime(calendar, DateTimeUtils.dateFormat_yyyy_MM) + "-";
        ArrayList<String> scheduleDateList = new ArrayList<>();
        for (Mother patient : patientList) {
            if (patient != null && patient.getVisits() != null && patient.getVisits().size() > 0) {
                for (Visit visitOld : patient.getVisits()) {
                    if (visitOld.getSchedDate().contains(strYyyy_MM))
                        scheduleDateList.add(visitOld.getSchedDate());
                }
            }

        }

        // Schedule dates are marked in calender
        compactCalendarView.removeAllEvents();
        int eventColor = Color.parseColor("#FFFFEB3B");
        List<Event> eventList = new ArrayList<>();
        Event event;
        for (String date : scheduleDateList) {
            if (date != null && date.length() > 0) {
                Calendar cal = DateTimeUtils.convertStringDateTimeToCalender(date, DateTimeUtils.dateFormat_yyyy_MM_dd);
                event = new Event(eventColor, cal.getTimeInMillis(), "Event at " + DateTimeUtils.convertCalenderToStringDateTime(cal, DateTimeUtils.dateFormat_dd_MMM_yyyy));
                eventList.add(event);
            }
        }
        compactCalendarView.addEvents(eventList);
        compactCalendarView.invalidate();
    }


    private void loadScheduleRecordsOnSelectedDate(String selectedDate) {

        List<Mother> list = new ArrayList<>();
        for (Mother patient : patientList) {
            if (patient != null && patient.getVisits() != null && patient.getVisits().size() > 0) {
                for (Visit visit : patient.getVisits()) {
                    if (visit.getSchedDate().equals(selectedDate))
                        list.add(patient);
                }
            }
        }

        adapter.setPatients(list);
        adapter.notifyDataSetChanged();
        // No visits to show / hide
        txtNoVisitsPlanned.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        txtNoOfVisits.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
        String noOfVisit = "No of visits : " + list.size();
        txtNoOfVisits.setText(noOfVisit);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called");
        if (queryListenerToken != null && query != null) {
            query.removeChangeListener(queryListenerToken);
            Log.d(TAG, "onDestroy - Listener removed");
        }
    }

}
