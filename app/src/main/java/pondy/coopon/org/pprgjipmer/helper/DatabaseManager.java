package pondy.coopon.org.pprgjipmer.helper;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.DataSource;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Dictionary;
import com.couchbase.lite.Document;
import com.couchbase.lite.Expression;
import com.couchbase.lite.Meta;
import com.couchbase.lite.MutableDocument;
import com.couchbase.lite.Ordering;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryBuilder;
import com.couchbase.lite.Result;
import com.couchbase.lite.SelectResult;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pondy.coopon.org.pprgjipmer.model.CheckList;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Subject;
import pondy.coopon.org.pprgjipmer.model.Task;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.model.VisitQuestion;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;

public class DatabaseManager {

    private final static String DATABASE_NAME = "phc_jipmer";
    private static final String TAG = "DatabaseManager";

    public Database database;
    private static DatabaseManager instance = null;

    private SharedPref sharedPref;

    // Patient Type
    private String K_TYPE_MOTHER = "mother";

    // Variable
    private String K_ID = "id";
    private String K_NAME = "name";
    private String K_TYPE = "type";
    private String K_AGE = "age";
    private String K_BLOOD_GROUP = "blood_group";
    private String K_PHONE = "phone";
    private String K_DOOR_NO = "dr_no";
    private String K_STREET_NAME = "street_name";
    private String K_AREA = "area";
    private String K_CITY = "city";
    private String K_PIN_CODE = "pin_code";
    private String K_LOCATION = "location";
    private String K_PATIENT_ID = "patient_id";
    private String K_DELIVERY_DATE = "delivery_date";
    private String K_CHILD_WEIGHT = "child_weight";
    private String K_CHILD_GENDER = "child_gender";
    private String K_CURRENT_VISIT = "current_visit";
    private String K_VISIT_INFO = "visits";
    private String K_COMPLETED = "completed";
    private String K_CHECK_LIST = "check_list";
    private String K_BABY = "baby";
    private String K_MOTHER = "mother";
    private String K_ASK = "ask";
    private String K_EXAMINE = "examine";
    private String K_COUNSEL = "counsel";
    private String K_SCHEDULE_DATE = "schedule_date";
    private String K_VISITED_DATE = "visited_date";
    private String K_VISITED_LOCATION = "visited_location";
    private String K_QUESTION = "question";
    private String K_ANSWER = "answer";
    private String K_WORKER = "worker";
    private String K_CREATE_AT = "create_at";
    private String K_UPDATE_AT = "update_at";
    private static final String K_MODIFIED = "is_modified";


    /* constructor */
    private DatabaseManager(Context context) {
        try {
            Log.d(TAG, "Setting Database Configuration");

            // set database configuration
            DatabaseConfiguration dbConfig = new DatabaseConfiguration(context);
            database = new Database(DATABASE_NAME, dbConfig);
            sharedPref = new SharedPref(context);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public static DatabaseManager getSharedInstance(Context context) {
        if (instance == null)
            instance = new DatabaseManager(context);
        return instance;
    }

    public static void clearInstance() {
        if (instance != null) {
            Log.d(TAG, "Clearing Database Instance");
            instance = null;
        }
    }

    public void createPatient(Mother patient) throws CouchbaseLiteException {
        MutableDocument masterPatient = patient.getMainId() != null ? getPatientMutableDocument(patient.getMainId()) : new MutableDocument();

        // Crete mode - first time of record creation
        Log.e(TAG, " PATIENT RECORD ID " + patient.getMainId());
        if (patient.getMainId() == null) {
            // patient meta data
            masterPatient.setString(K_TYPE, "mother");
            masterPatient.setInt(K_CURRENT_VISIT, patient.getCurrentVisit());
            masterPatient.setDate(K_CREATE_AT, new Date());
        }

        // Common
        masterPatient.setString(K_NAME, patient.getName());
        masterPatient.setInt(K_AGE, patient.getAge());
        masterPatient.setString(K_PHONE, patient.getPhone());
        masterPatient.setString(K_BLOOD_GROUP, patient.getBloodGroup());
        masterPatient.setString(K_DOOR_NO, patient.getDrNo());
        masterPatient.setString(K_STREET_NAME, patient.getStreetName());
        masterPatient.setString(K_AREA, patient.getArea());
        masterPatient.setString(K_CITY, patient.getCity());
        masterPatient.setString(K_PIN_CODE, patient.getPinCode());
        masterPatient.setString(K_WORKER, patient.getWorker());
        masterPatient.setInt(K_PATIENT_ID, patient.getMotherId());
        if(patient.getModified() == null)
            patient.setModified(false);
        masterPatient.setBoolean(K_MODIFIED, patient.getModified());

        // Location
        if (patient.getLocation() != null) {
            masterPatient.setValue(K_LOCATION, patient.getLocation());
        }

        // After 1st visit not allow to edit delivery date & child information
        if (patient.getCurrentVisit() != null) {
            // Date of delivery
            masterPatient.setString(K_DELIVERY_DATE, patient.getDeliveryDate());
            masterPatient.setString(K_CHILD_GENDER, patient.getChildGender());
            masterPatient.setDouble(K_CHILD_WEIGHT, patient.getChildWeight());


            // Visit information
            if (patient.getVisits() != null && patient.getVisits().size() > 0) {
                List<Map<String, Object>> visitInfoObj = new ArrayList<>();
                Map<String, Object> visitObj;
                for (Visit visit :  patient.getVisits()) {

                    visitObj = new HashMap<>();

                    visitObj.put(K_ID, visit.getId());
                    visitObj.put(K_COMPLETED, visit.getCompleted());
                    visitObj.put(K_SCHEDULE_DATE, visit.getSchedDate());
                    visitObj.put(K_VISITED_DATE, visit.getVisitedDate());
                    visitObj.put(K_VISITED_LOCATION, visit.getVisitedLocation());


                    Map<String , Object> babyCheckListObj = new HashMap<>();
                    if(visit.getChecklist() != null && visit.getChecklist().getBaby() != null){

                        Subject baby = visit.getChecklist().getBaby();
                        if(baby != null) {
                            babyCheckListObj.put(K_ASK, buildTaskToMap(baby.getAsk()));
                            babyCheckListObj.put(K_EXAMINE, buildTaskToMap(baby.getExamine()));
                            babyCheckListObj.put(K_COUNSEL, buildTaskToMap(baby.getCounsel()));
                        }
                    }

                    Map<String , Object> motherCheckListObj = new HashMap<>();
                    if(visit.getChecklist() != null && visit.getChecklist().getMother() != null){
                        Subject mother = visit.getChecklist().getMother();
                        if(mother !=null) {
                            motherCheckListObj.put(K_ASK, buildTaskToMap(mother.getAsk()));
                            motherCheckListObj.put(K_EXAMINE, buildTaskToMap(mother.getExamine()));
                            motherCheckListObj.put(K_COUNSEL, buildTaskToMap(mother.getCounsel()));
                        }
                    }

                    Map<String, Map<String, Object>> checkList = new HashMap<>();
                    checkList.put(K_BABY, babyCheckListObj);
                    checkList.put(K_MOTHER, motherCheckListObj);

                    visitObj.put(K_CHECK_LIST, checkList);

                    visitInfoObj.add(visitObj);
                }
                masterPatient.setValue(K_VISIT_INFO, visitInfoObj);
            } else
                masterPatient.setValue(K_VISIT_INFO, null);
        }

        masterPatient.setDate(K_UPDATE_AT, new Date());

        database.save(masterPatient);
    }

    private List<Map<String, Object>> buildTaskToMap(List<Task> taskList) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        if(taskList != null) {
            for (Task task : taskList) {
                Map<String, Object> map = new HashMap<>();
                map.put(K_ID, task.getId());
                map.put(K_QUESTION, task.getQuestion());
                map.put(K_ANSWER, task.getAnswer());
                mapList.add(map);
            }
        }

        return mapList;
    }

    /**
     * Mutable document need to send when edit the document
     *
     * @param patientId - Id value of patient
     * @return
     */
    private MutableDocument getPatientMutableDocument(String patientId) {
        return database.getDocument(patientId).toMutable();
    }

    public Mother getPatient(String patientId) {
        Document patientDoc = database.getDocument(patientId);
        Mother patient = buildPatient(patientDoc);
        patient.setMainId(patientId);
        return patient;
    }

    public Mother buildPatient(Object obj) {
        Mother patient = new Mother();
        if (obj instanceof Document) {
            Document patientDoc = (Document) obj;
            patient.setName(patientDoc.getString(K_NAME));
            patient.setType(patientDoc.getString(K_TYPE));
            patient.setAge(patientDoc.getInt(K_AGE));
            patient.setBloodGroup(patientDoc.getString(K_BLOOD_GROUP));
            patient.setPhone(patientDoc.getString(K_PHONE));
            patient.setDrNo(patientDoc.getString(K_DOOR_NO));
            patient.setStreetName(patientDoc.getString(K_STREET_NAME));
            patient.setArea(patientDoc.getString(K_AREA));
            patient.setCity(patientDoc.getString(K_CITY));
            patient.setPinCode(patientDoc.getString(K_PIN_CODE));
            patient.setMotherId(patientDoc.getInt(K_PATIENT_ID));
            patient.setLocation(patientDoc.getString(K_LOCATION));
            patient.setModified(patientDoc.getBoolean(K_MODIFIED));

            patient.setDeliveryDate(patientDoc.getString(K_DELIVERY_DATE));
            patient.setChildGender(patientDoc.getString(K_CHILD_GENDER));
            patient.setChildWeight(patientDoc.getDouble(K_CHILD_WEIGHT));
            patient.setWorker(patientDoc.getString(K_WORKER));

            patient.setCurrentVisit(patientDoc.getInt(K_CURRENT_VISIT));
            patient.setCreateAt(DateTimeUtils.convertDateToString(patientDoc.getDate(K_CREATE_AT), DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));
            patient.setUpdateAt(DateTimeUtils.convertDateToString(patientDoc.getDate(K_UPDATE_AT), DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));


            List<Visit> visitInfoList = new ArrayList<>();
            if(patientDoc.contains(K_VISIT_INFO) && patientDoc.getArray(K_VISIT_INFO) != null) {
                List visitArray = patientDoc.getArray(K_VISIT_INFO).toList();
                if (visitArray != null && visitArray.size() > 0) {
                    for (Object objVisit : visitArray) {

                        Map<String, Object> mapVisit = (Map<String, Object>) objVisit;
                        Visit visit = new Visit();
                        if (mapVisit != null) {
                            if (mapVisit.containsKey(K_ID))
                                visit.setId((Long) mapVisit.get(K_ID));
                            if (mapVisit.containsKey(K_COMPLETED))
                                visit.setCompleted((Boolean) mapVisit.get(K_COMPLETED));
                            else
                                visit.setCompleted(false);
                            if (mapVisit.containsKey(K_SCHEDULE_DATE))
                                visit.setSchedDate((String) mapVisit.get(K_SCHEDULE_DATE));
                            if (mapVisit.containsKey(K_VISITED_DATE))
                                visit.setVisitedDate((String) mapVisit.get(K_VISITED_DATE));
                            else
                                visit.setVisitedDate("");

                            if (mapVisit.containsKey(K_VISITED_LOCATION))
                                visit.setVisitedLocation((String) mapVisit.get(K_VISITED_LOCATION));
                            else
                                visit.setVisitedLocation("");

                            CheckList checkList = new CheckList();

                            if (mapVisit.containsKey(K_CHECK_LIST)) {

                                Map<String, Object> mapCheckList = (Map<String, Object>) mapVisit.get(K_CHECK_LIST);

                                if (mapCheckList != null) {
                                    if (mapCheckList.containsKey(K_BABY)) {
                                        checkList.setBaby(convertMapToSubject((Map<String, Object>) mapCheckList.get(K_BABY)));
                                    }

                                    if (mapCheckList.containsKey(K_MOTHER)) {
                                        checkList.setMother(convertMapToSubject((Map<String, Object>) mapCheckList.get(K_MOTHER)));
                                    }
                                }
                            }

                            visit.setChecklist(checkList);
                            visitInfoList.add(visit);
                        }
                    }
                }
            }

            patient.setVisits(visitInfoList.size() > 0 ? visitInfoList : null);

        } else if (obj instanceof Result) {
            Result result = (Result) obj;
            patient.setMainId(result.toList().get(1).toString());
            Dictionary patientDoc = result.getDictionary(database.getName());

            patient.setName(patientDoc.getString(K_NAME));
            patient.setType(patientDoc.getString(K_TYPE));
            patient.setAge(patientDoc.getInt(K_AGE));
            patient.setBloodGroup(patientDoc.getString(K_BLOOD_GROUP));
            patient.setPhone(patientDoc.getString(K_PHONE));
            patient.setDrNo(patientDoc.getString(K_DOOR_NO));
            patient.setStreetName(patientDoc.getString(K_STREET_NAME));
            patient.setArea(patientDoc.getString(K_AREA));
            patient.setCity(patientDoc.getString(K_CITY));
            patient.setPinCode(patientDoc.getString(K_PIN_CODE));
            patient.setMotherId(patientDoc.getInt(K_PATIENT_ID));
            patient.setLocation(patientDoc.getString(K_LOCATION));
            patient.setModified(patientDoc.getBoolean(K_MODIFIED));

            patient.setDeliveryDate(patientDoc.getString(K_DELIVERY_DATE));
            patient.setChildGender(patientDoc.getString(K_CHILD_GENDER));
            patient.setChildWeight(patientDoc.getDouble(K_CHILD_WEIGHT));
            patient.setWorker(patientDoc.getString(K_WORKER));

            patient.setCurrentVisit(patientDoc.getInt(K_CURRENT_VISIT));
            patient.setCreateAt(DateTimeUtils.convertDateToString(patientDoc.getDate(K_CREATE_AT), DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));
            patient.setUpdateAt(DateTimeUtils.convertDateToString(patientDoc.getDate(K_UPDATE_AT), DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));

            List<Visit> visitInfoList = new ArrayList<>();
            if(patientDoc.contains(K_VISIT_INFO) && patientDoc.getArray(K_VISIT_INFO) != null) {
                List visitArray = patientDoc.getArray(K_VISIT_INFO).toList();
                if (visitArray != null && visitArray.size() > 0) {
                    for (Object objVisit : visitArray) {

                        Map<String, Object> mapVisit = (Map<String, Object>) objVisit;
                        Visit visit = new Visit();
                        if (mapVisit != null) {
                            if (mapVisit.containsKey(K_ID))
                                visit.setId((Long) mapVisit.get(K_ID));
                            if (mapVisit.containsKey(K_COMPLETED))
                                visit.setCompleted((Boolean) mapVisit.get(K_COMPLETED));
                            else
                                visit.setCompleted(false);
                            if (mapVisit.containsKey(K_SCHEDULE_DATE))
                                visit.setSchedDate((String) mapVisit.get(K_SCHEDULE_DATE));
                            if (mapVisit.containsKey(K_VISITED_DATE))
                                visit.setVisitedDate((String) mapVisit.get(K_VISITED_DATE));
                            else
                                visit.setVisitedDate("");

                            if (mapVisit.containsKey(K_VISITED_LOCATION))
                                visit.setVisitedLocation((String) mapVisit.get(K_VISITED_LOCATION));
                            else
                                visit.setVisitedLocation("");

                            CheckList checkList = new CheckList();

                            if (mapVisit.containsKey(K_CHECK_LIST)) {

                                Map<String, Object> mapCheckList = (Map<String, Object>) mapVisit.get(K_CHECK_LIST);

                                if (mapCheckList != null) {
                                    if (mapCheckList.containsKey(K_BABY)) {
                                        checkList.setBaby(convertMapToSubject((Map<String, Object>) mapCheckList.get(K_BABY)));
                                    }

                                    if (mapCheckList.containsKey(K_MOTHER)) {
                                        checkList.setMother(convertMapToSubject((Map<String, Object>) mapCheckList.get(K_MOTHER)));
                                    }
                                }
                            }

                            visit.setChecklist(checkList);
                            visitInfoList.add(visit);
                        }
                    }
                }
            }
            patient.setVisits(visitInfoList.size() > 0 ? visitInfoList : null);
        }
        return patient;
    }

    private Subject convertMapToSubject(Map<String, Object> mapSubject) {
        Subject subject = new Subject();
        if(mapSubject != null) {
            subject.setAsk(convertMapToTask((List<Map<String, Object>>) mapSubject.get(K_ASK)));
            subject.setExamine(convertMapToTask((List<Map<String, Object>>) mapSubject.get(K_EXAMINE)));
            subject.setCounsel(convertMapToTask((List<Map<String, Object>>) mapSubject.get(K_COUNSEL)));
        }
        return subject;
    }

    private List<Task> convertMapToTask(List<Map<String, Object>> listType) {
        List<Task> list = new ArrayList<>();

        if(listType != null){
            for(Map<String, Object> mapType : listType){
                list.add(new Task((Long) mapType.get(K_ID), (String) mapType.get(K_QUESTION), (Boolean) mapType.get(K_ANSWER)));
            }
        }
        return list;
    }


    /**
     * @param mainId      - Patient record of update record
     * @param isVisitCompleted - Is true mean completed current visit. Other wise no
     * @param updateVisit
     * @throws CouchbaseLiteException
     */
    public void updateVisitStatus(String mainId, Visit updateVisit, boolean isVisitCompleted) throws CouchbaseLiteException {

        // Get mutable document of particular patient id
        MutableDocument masterPatient = getPatientMutableDocument(mainId);

        // Get current visit
        int currentVisit = masterPatient.getInt(K_CURRENT_VISIT);

        // If visit completed update current visit plus 1
        if (isVisitCompleted)
        {
            masterPatient.setInt(K_CURRENT_VISIT, currentVisit + 1);
            updateVisit.setCompleted(true);
        }

        // Getting visit information as mutable document
        List visitList = masterPatient.getArray(K_VISIT_INFO).toMutable().toList();

        Map<String, Object> visitInfoDic = (Map<String, Object>) visitList.get(currentVisit-1);

        visitInfoDic.put(K_COMPLETED, updateVisit.getCompleted());
        visitInfoDic.put(K_VISITED_DATE, DateTimeUtils.dateNow(DateTimeUtils.dateFormat_yyyy_MM_dd));
        visitInfoDic.put(K_VISITED_LOCATION, updateVisit.getVisitedLocation());


        Map<String , Object> babyCheckListObj = new HashMap<>();
        if(updateVisit.getChecklist() != null && updateVisit.getChecklist().getBaby() != null){

            Subject baby = updateVisit.getChecklist().getBaby();
            if(baby != null) {
                babyCheckListObj.put(K_ASK, buildTaskToMap(baby.getAsk()));
                babyCheckListObj.put(K_EXAMINE, buildTaskToMap(baby.getExamine()));
                babyCheckListObj.put(K_COUNSEL, buildTaskToMap(baby.getCounsel()));
            }
        }

        Map<String , Object> motherCheckListObj = new HashMap<>();
        if(updateVisit.getChecklist() != null && updateVisit.getChecklist().getMother() != null){
            Subject mother = updateVisit.getChecklist().getMother();
            if(mother !=null) {
                motherCheckListObj.put(K_ASK, buildTaskToMap(mother.getAsk()));
                motherCheckListObj.put(K_EXAMINE, buildTaskToMap(mother.getExamine()));
                motherCheckListObj.put(K_COUNSEL, buildTaskToMap(mother.getCounsel()));
            }
        }

        Map<String, Map<String, Object>> checkList = new HashMap<>();
        checkList.put(K_BABY, babyCheckListObj);
        checkList.put(K_MOTHER, motherCheckListObj);

        visitInfoDic.put(K_CHECK_LIST, checkList);

        // Update in visit info
        masterPatient.setValue(K_VISIT_INFO, visitList);

        // Update date
        masterPatient.setDate(K_UPDATE_AT, new Date());
        masterPatient.setBoolean(K_MODIFIED, true);
        database.save(masterPatient);
    }

    public Query getAllPatients() {
        return QueryBuilder.select(SelectResult.all(), SelectResult.expression(Meta.id))
                .from(DataSource.database(database))
                .where(Expression.property(K_TYPE).equalTo(Expression.string(K_TYPE_MOTHER)))
                .orderBy(Ordering.property(K_CURRENT_VISIT).ascending());
    }

    public Query getMotherMainId(Integer motherId){
        return QueryBuilder.select(
                SelectResult.expression(Meta.id),
                SelectResult.expression(Expression.property(K_PATIENT_ID)))
                .from(DataSource.database(database))
                .where(Expression.property(K_TYPE).equalTo(Expression.string(K_TYPE_MOTHER)).and(Expression.property(K_PATIENT_ID).equalTo(Expression.intValue(motherId))));
    }

    public Query getScheduleRecordsOnParticularDate(String selectedDate) {
        Log.e("TAG", "NOTIFICATION -- " + selectedDate);
        return QueryBuilder
                .select(SelectResult.all(), SelectResult.expression(Meta.id))
                .from(DataSource.database(database))
                .where(Expression.property(K_TYPE).equalTo(Expression.string(K_TYPE_MOTHER))
                       .and(Expression.parameter(K_VISIT_INFO + "[*]." + K_SCHEDULE_DATE).like(Expression.string(selectedDate+"%"))));
    }
}