package pondy.coopon.org.pprgjipmer.utils.constant;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;

public class PatientRecordConst {

    public static final int BASE = 0;
    public static final int MAP = 1;
    public static final int SCHEDULE = 2;

    @IntDef({BASE, SCHEDULE, MAP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Season {
    }
}