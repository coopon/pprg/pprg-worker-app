package pondy.coopon.org.pprgjipmer.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.couchbase.lite.CouchbaseLiteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import info.hoang8f.android.segmented.SegmentedGroup;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.VisitQuestion;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.GPSTracker;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import pondy.coopon.org.pprgjipmer.view.CustomDateTimePicker;

public class AddEditBioDataActivity extends AppCompatActivity {

    private static final String TAG = "AddEditBioDataActivity";

    // Helper
    private SharedPref sharedPref;
    private DatabaseManager dbMgr;
    private GPSTracker gpsTracker;

    // Variables
    private Location currentBestLocation;
    private Calendar dateOfDelivery = null;
    private String[] bloodGroups;
    private Boolean isEditMode = false;
    private Mother masterPatient = new Mother();
    private Random random = new Random();

    // Views
    private AlertDialog alert;
    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_age)
    EditText editAge;
    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.edit_addr_dr_no)
    EditText editDrNo;
    @BindView(R.id.edit_addr_street_name)
    EditText editStreetName;
    @BindView(R.id.edit_addr_area)
    EditText editArea;
    @BindView(R.id.edit_addr_city)
    EditText editCity;
    @BindView(R.id.edit_addr_pincode)
    EditText editPinCode;
    @BindView(R.id.edit_location)
    EditText editLocation;
    @BindView(R.id.edit_patient_id)
    EditText editPatientId;
    @BindView(R.id.edit_delivery_date)
    EditText editDeliveryDate;
    @BindView(R.id.edit_child_weight)
    EditText editChildWeight;
    @BindView(R.id.spinner_blood_group)
    AppCompatSpinner spinnerBloodGroup;
    @BindView(R.id.seg_child_gender)
    SegmentedGroup rgChildGender;
    @BindView(R.id.layout_child_information)
    LinearLayout layoutChildInformation;
    @BindView(R.id.smooth_progress_bar)
    SmoothProgressBar progressBar;
    @BindView(R.id.btn_save)
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_bio_data);

        ButterKnife.bind(this);

        sharedPref = new SharedPref(getApplicationContext());
        dbMgr = DatabaseManager.getSharedInstance(this);
        gpsTracker = new GPSTracker(this);

        // Init views
        init();


        /*
         * Check Create or Edit mode
         */
        Intent incoming = getIntent();
        if (incoming.getStringExtra("PATIENT_ID") != null) // Edit mode - presence of PATIENT_ID means, user is here to edit
        {
            isEditMode = true;

            btnSave.setText(getString(R.string.btn_update));
            setTitle(getString(R.string.title_activity_edit_bio_data));

            String patientID = incoming.getStringExtra("PATIENT_ID");
            if (!patientID.isEmpty()) {
                masterPatient = dbMgr.getPatient(patientID); // Load master data from database for edit

                masterPatient.setMainId(patientID);
                editName.setText(masterPatient.getName() != null ? masterPatient.getName() : "");
                editAge.setText(String.valueOf(masterPatient.getAge() != null ? masterPatient.getAge() : 0));
                editPhone.setText(masterPatient.getPhone() != null ? masterPatient.getPhone() : "");
                editDrNo.setText(masterPatient.getDrNo() != null ? masterPatient.getDrNo() : "");
                editStreetName.setText(masterPatient.getStreetName() != null ? masterPatient.getStreetName() : "");
                editArea.setText(masterPatient.getArea() != null ? masterPatient.getArea() : "");
                editCity.setText(masterPatient.getCity() != null ? masterPatient.getCity() : "");
                editPinCode.setText(masterPatient.getPinCode() != null ? masterPatient.getPinCode() : "");
                editPatientId.setText(masterPatient.getMotherId() != null ? masterPatient.getMotherId().toString() : "");

                if (masterPatient.getDeliveryDate() != null) {
                    dateOfDelivery = DateTimeUtils.convertStringDateTimeToCalender(masterPatient.getDeliveryDate(), DateTimeUtils.dateFormat_YYYY_MM_dd_hh_mm_ss);
                    editDeliveryDate.setText(DateTimeUtils.convertCalenderToStringDateTime(dateOfDelivery, DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));

                    rgChildGender.check(masterPatient.getChildGender() != null && masterPatient.getChildGender().equalsIgnoreCase("male") ? R.id.radio_male : R.id.radio_female);
                    editChildWeight.setText(String.valueOf(masterPatient.getChildWeight()));
                } else
                    dateOfDelivery = null;

                if (masterPatient.getLocation() != null) {
                    Location location = GeneralUtils.convertStringToLocation(masterPatient.getLocation());
                    if(location != null) {
                        String loc = "lat : " + String.format(Locale.ENGLISH, "%.4f", location.getLatitude()) + ", "
                                + "lon : " + String.format(Locale.ENGLISH, "%.4f", location.getLongitude());
                        editLocation.setText(loc);
                    }else
                        editLocation.setText("");
                }
            }
        }

        // Init view listener, adapter and configurations
        loadView();

        btnSave.setOnClickListener(view -> {

            if (isValidFields()) {

                // Stop Location listener
                gpsTracker.stopLocationListener();

                // Crete mode - first time of record creation
                if (!isEditMode) {
                    // patient meta data
                    //masterPatient.setType("mother");
                    masterPatient.setCurrentVisit(1);
                    // Worker channels
                    //masterPatient.setWorkerChannels(sharedPref.getChannels());
                    //masterPatient.setCreateAt(new Date());
                }


                // Common
                masterPatient.setName(editName.getText().toString());
                masterPatient.setAge(Integer.valueOf(editAge.getText().toString()));
                masterPatient.setPhone(editPhone.getText().toString());
                masterPatient.setDrNo(editDrNo.getText().toString());
                masterPatient.setStreetName(editStreetName.getText().toString());
                masterPatient.setArea(editArea.getText().toString());
                masterPatient.setCity(editCity.getText().toString());
                masterPatient.setPinCode(editPinCode.getText().toString());
                //masterPatient.setMotherId(editPatientId.getText().toString());
                masterPatient.setLocation(currentBestLocation.getLatitude() + "," +currentBestLocation.getLongitude());

                // Date of delivery
                masterPatient.setDeliveryDate(dateOfDelivery != null ? DateTimeUtils.convertCalenderToStringDateTime(dateOfDelivery, DateTimeUtils.dateFormat_YYYY_MM_dd_hh_mm_ss) : null);

                /*
                 * Delivery date changes affect the visits
                 * Schedule / Reschedule the visits based on delivery date and child weight
                 */
                if (dateOfDelivery != null) {
                    masterPatient.setChildGender(((RadioButton) findViewById(rgChildGender.getCheckedRadioButtonId())).getText().toString());
                    double childWeight = Double.parseDouble(editChildWeight.getText().toString());
                    masterPatient.setChildWeight(Double.parseDouble(editChildWeight.getText().toString()));

                    //Schedule the visit date, visit based questions and answers are added depends on weight
                    //masterPatient.setVisits(getVisitList(dateOfDelivery, childWeight));
                } else {
                    masterPatient.setChildGender(null);
                    masterPatient.setChildWeight(0.0);
                    //masterPatient.setVisitsInfo(null);
                }

                // Update date - every changes
                //masterPatient.setUpdateAt(new Date());

                try {
                    dbMgr.createPatient(masterPatient);
                    ToastUtils.showSuccess(getApplicationContext(), String.format(getString(isEditMode ? R.string.msg_patient_updated : R.string.msg_patient_created), masterPatient.getName()));
                    if (isEditMode)
                        finish();
                    else
                        resetFields();
                } catch (CouchbaseLiteException e) {
                    Log.e(TAG, "onClick: Exception Occurred: " + e);
                    e.printStackTrace();
                    ToastUtils.showError(getApplicationContext(), e.getMessage());
                }
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void loadView() {

        /*
         * Blood group adapter
         */
        bloodGroups = getResources().getStringArray(R.array.blood_groups);
        // Array Adapter for Blood Group Spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, bloodGroups);

        // Drop down layout style - list view with radio button
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerBloodGroup.setAdapter(adapter);

        spinnerBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                masterPatient.setBloodGroup(position > 0 ? bloodGroups[position] : null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // If edit mode - default select
        if (masterPatient.getBloodGroup() != null) {
            int selectedPos = 0;
            for (int b = 0; b < bloodGroups.length; b++)
                if (bloodGroups[b].equals(masterPatient.getBloodGroup()))
                    selectedPos = b;
            spinnerBloodGroup.setSelection(selectedPos);
        }

        /*
         * Location listener
         */
        editLocation.setOnClickListener(view -> {

            // Check location data exist to remove. I.e recall
            if (!TextUtils.isEmpty(editLocation.getText())) {
                currentBestLocation = null;
                editLocation.setText("");
            }

            if (gpsTracker != null) {
                gpsTracker.initLocationManager(); // Init location manager
                callLocationListener(); // Call location listener
            }

        });

        /*
         * Right clear drawable click to clear - Location
         */
        editLocation.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                // Right drawable
                if (event.getRawX() >= (editLocation.getRight() - editLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    editLocation.setText("");
                    currentBestLocation = null;
                    progressBar.setVisibility(View.GONE);

                    // Destroy location manager
                    gpsTracker.destroyLocationManager();

                    return true;
                }
            }
            return false;
        });

        // Date and time date picker init
        final CustomDateTimePicker custom = new CustomDateTimePicker(this,
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                        if (calendarSelected.before(Calendar.getInstance()) || calendarSelected.equals(Calendar.getInstance())) // Check valid date or not
                        {
                            dateOfDelivery = calendarSelected;
                            editDeliveryDate.setText("");
                            editDeliveryDate.setText(DateTimeUtils.convertCalenderToStringDateTime(dateOfDelivery, DateTimeUtils.dateFormat_d_MMM_YYYY_hh_mm_ss_a));
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.error_invalid_delivery_date), Toast.LENGTH_LONG).show();
                            editDeliveryDate.setText("");
                            dateOfDelivery = null;
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
        /*
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        custom.set24HourFormat(false);
        /*
         * Pass Directly current data and time to show when it pop up
         */
        custom.setDate(dateOfDelivery != null ? dateOfDelivery : Calendar.getInstance());

        /*
         * Click to show date picker dialog
         */
        editDeliveryDate.setOnClickListener(v -> custom.showDialog());

        editDeliveryDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                layoutChildInformation.setVisibility(!TextUtils.isEmpty(editDeliveryDate.getText()) ? View.VISIBLE : View.GONE);
            }
        });

        layoutChildInformation.setVisibility(!TextUtils.isEmpty(editDeliveryDate.getText()) ? View.VISIBLE : View.GONE);


        /*
         * Right clear drawable click to clear - Delivery date
         */
        editDeliveryDate.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                // Right drawable
                if (event.getRawX() >= (editDeliveryDate.getRight() - editDeliveryDate.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    editDeliveryDate.setText("");
                    dateOfDelivery = null;
                    return true;
                }
            }
            return false;
        });

        // After 1st visit not allow to edit delivery date & child information
        if (masterPatient.getCurrentVisit() != null && masterPatient.getCurrentVisit() > 1) {
            editDeliveryDate.setVisibility(View.GONE);
            layoutChildInformation.setVisibility(View.GONE);
        }
    }

    private void callLocationListener() {

        if (gpsTracker != null) {
            // Show loading progress
            if (gpsTracker.checkLocationPermission() && gpsTracker.isLocationEnabled())
                progressBar.setVisibility(View.VISIBLE);

            // Start the location listener
            gpsTracker.startLocationListener(locationListener);
        }
    }

    /**
     * Init
     */
    private void init() {
        setTitle(getString(R.string.title_activity_add_bio_data));
        editName.requestFocus();
    }

    /**
     * Location listener
     */
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            if (gpsTracker.isBetterLocation(location, currentBestLocation)) {
                currentBestLocation = location;

                // stop progress bar
                progressBar.setVisibility(View.GONE);
            }

            if (currentBestLocation != null) {
                masterPatient.setLocation(currentBestLocation.getLatitude() + "," + currentBestLocation.getLongitude());
                String loc = "lat : " + String.format(Locale.ENGLISH, "%.4f", currentBestLocation.getLatitude()) + ", "
                        + "lon : " + String.format(Locale.ENGLISH, "%.4f", currentBestLocation.getLongitude());
                editLocation.setText(loc);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPSTracker.CODE_GPS_ENABLE && gpsTracker != null && gpsTracker.isLocationEnabled()) {
            callLocationListener();
        }
    }

    private boolean isValidFields() {

        if (!TextUtils.isEmpty(editName.getText())) {
            if (!TextUtils.isEmpty(editAge.getText())) {
                if (masterPatient.getBloodGroup() != null) {
                    if (!TextUtils.isEmpty(editPhone.getText())) {
                        if (!TextUtils.isEmpty(editDrNo.getText())) {
                            if (!TextUtils.isEmpty(editStreetName.getText())) {
                                if (!TextUtils.isEmpty(editArea.getText())) {
                                    if (!TextUtils.isEmpty(editCity.getText())) {
                                        if (!TextUtils.isEmpty(editPinCode.getText())) {
                                            if (dateOfDelivery == null)
                                                return true;
                                            else {
                                                if (dateOfDelivery.before(Calendar.getInstance()) || dateOfDelivery.equals(Calendar.getInstance())) // Check valid date of delivery
                                                {
                                                    if (!TextUtils.isEmpty(editChildWeight.getText()) && Double.valueOf(editChildWeight.getText().toString()) <= 5) // Child weight not more than 5kg
                                                        return true;
                                                    else
                                                        ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_child_weight));
                                                } else {
                                                    ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_delivery_date));
                                                    editDeliveryDate.setText("");
                                                    dateOfDelivery = null;
                                                }
                                            }
                                        } else
                                            ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_pin_code));
                                    } else
                                        ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_city));
                                } else
                                    ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_area));
                            } else
                                ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_street_name));
                        } else
                            ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_door_no));
                    } else
                        ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_patient_phone_no));
                } else
                    ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_patient_blood_grp));
            } else
                ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_patient_age));
        } else {
            ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_patient_name));
        }

        return false;
    }

    private void resetFields() {
        editName.setText("");
        editAge.setText("");
        editPhone.setText("");
        editDrNo.setText("");
        editStreetName.setText("");
        editCity.setText("");
        editArea.setText("");
        editPinCode.setText("");
        editLocation.setText("");
        progressBar.setVisibility(View.GONE);
        editPatientId.setText("");
        editDeliveryDate.setText("");
        editChildWeight.setText("");
        currentBestLocation = null;
        dateOfDelivery = null;
        spinnerBloodGroup.setSelection(0);
        rgChildGender.getChildAt(0).setSelected(true);
        editName.requestFocus();

        if (gpsTracker != null)
            gpsTracker.destroyLocationManager();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (gpsTracker.isLocationEnabled())
            callLocationListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gpsTracker.stopLocationListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gpsTracker.destroyLocationManager();
    }

    /*private List<VisitOld> getVisitList(Calendar dateOfDelivery, double childWeight) {
        List<VisitOld> visitOldList = new ArrayList<>();

        int randVal = 0;

        //https://www.javacodegeeks.com/2013/10/android-json-tutorial-create-and-parse-json-data.html
        if (childWeight <= 2.499) // Low weight child
        {
            *//*
             * VisitOld 1 - Day 3
             *//*
            List<VisitQuestion> visit1Questions = new ArrayList<>();
            visit1Questions.add(new VisitQuestion("Kangaroo Mother care", false));
            visit1Questions.add(new VisitQuestion("Exclusive breastfeeding and alternative methods of feeding", false));
            visit1Questions.add(new VisitQuestion("Care of the newborn", false));

            randVal = 3;
            String visit1ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit1ScheduleDate, visit1Questions));


            *//*
             * VisitOld 2 - Day 7
             *//*
            List<VisitQuestion> visit2Questions = new ArrayList<>();
            visit2Questions.add(new VisitQuestion("Personal care of the mother and the child", false));
            visit2Questions.add(new VisitQuestion("Exclusive breastfeeding", false));

            randVal = 7;
            String visit2ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit2ScheduleDate, visit2Questions));


            *//*
             * VisitOld 3 - Day 14
             *//*
            List<VisitQuestion> visit3Questions = new ArrayList<>();
            visit3Questions.add(new VisitQuestion("Adherence to IFA tablets", false));
            visit3Questions.add(new VisitQuestion("Nutritional advice", false));

            randVal = 14;
            String visit3ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit3ScheduleDate, visit3Questions));


            *//*
             * VisitOld 4 - Within one month (Day 21 to 30)
             *//*
            List<VisitQuestion> visit4Questions = new ArrayList<>();
            visit4Questions.add(new VisitQuestion("Care of the newborn", false));
            visit4Questions.add(new VisitQuestion("Do's and Don'ts", false));

            randVal = getRandomValue(randVal, 21, 30);
            String visit4ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit4ScheduleDate, visit4Questions));


            *//*
             * VisitOld 5 - Fifth week (Day 31 to 35)
             *//*
            List<VisitQuestion> visit5Questions = new ArrayList<>();
            visit5Questions.add(new VisitQuestion("Monitoring of growth of newborn", false));

            randVal = getRandomValue(randVal, 31, 35);
            String visit5ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new Visit(visit5ScheduleDate, visit5Questions));


            *//*
             * VisitOld 6 -  Sixth week (Day 36 to 42)
             *//*
            List<VisitQuestion> visit6Questions = new ArrayList<>();
            visit6Questions.add(new VisitQuestion("Immunization", false));

            randVal = getRandomValue(randVal, 36, 42);
            String visit6ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit6ScheduleDate, visit6Questions));


        } else // Normal weight child
        {

            *//*
             * VisitOld 1 - With in 1st week (Day 1 to 6)
             *//*
            List<VisitQuestion> visit1Questions = new ArrayList<>();
            visit1Questions.add(new VisitQuestion("Personal hygiene", false));
            visit1Questions.add(new VisitQuestion("Exclusive breastfeeding", false));
            visit1Questions.add(new VisitQuestion("Nutritional advice", false));
            visit1Questions.add(new VisitQuestion("Care of the newborn", false));
            visit1Questions.add(new VisitQuestion("Adherence to Iron and Folic Acid supplementation", false));

            randVal = getRandomValue(randVal, 1, 6);
            String visit1ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit1ScheduleDate, visit1Questions));


            *//*
             * VisitOld 2 - 7 to 14 days after birth (Day 7 to 14)
             *//*
            List<VisitQuestion> visit2Questions = new ArrayList<>();
            visit2Questions.add(new VisitQuestion("Exclusive breastfeeding", false));
            visit2Questions.add(new VisitQuestion("Nutritional advice for the mothers", false));

            randVal = getRandomValue(randVal, 7, 14);
            String visit2ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit2ScheduleDate, visit2Questions));


            *//*
             * VisitOld 3 - 4 to 6 weeks after birth (Day 22 to 42)
             *//*
            List<VisitQuestion> visit3Questions = new ArrayList<>();
            visit3Questions.add(new VisitQuestion("Care of the newborn", false));
            visit3Questions.add(new VisitQuestion("Immunization of the baby", false));
            visit3Questions.add(new VisitQuestion("Exclusive breastfeeding", false));
            visit3Questions.add(new VisitQuestion("Nutritional advice for the mothers", false));

            randVal = 21; // One week gape (14 to 21)
            randVal = getRandomValue(randVal, 22, 40);
            String visit3ScheduleDate = DateTimeUtils.convertCalenderToStringDateTime(DateTimeUtils.addWorkingDaysToAlter(dateOfDelivery, randVal), DateTimeUtils.dateFormat_yyyy_MM_dd);

            visitOldList.add(new VisitOld(visit3ScheduleDate, visit3Questions));
        }

        return visitOldList;
    }*/

    private int getRandomValue(int prev, int min, int max) {
        int res = 0;

        // With in particular week need gape. So, minimum date increased by and also maximum date decreased by 1
        min = min + 1;
        max = max - 1;

        // Calculate different days between previous date and current minimum period
        int diff = min - prev;

        if (diff < 0)
            diff = 0;

        // Every alternative dates are need some gape. So, gape calculated depends on previous date
        min = diff > 4 ? min : min + (4 - diff);

        try {
            if (max >= min) // Check valid values
                res = random.nextInt(max - min + 1) + min; // Calculate random number between min and max value.
            else
                res = max;
        } catch (Exception e) {
            e.printStackTrace();
            res = min;
        }
        return res;
    }
}
