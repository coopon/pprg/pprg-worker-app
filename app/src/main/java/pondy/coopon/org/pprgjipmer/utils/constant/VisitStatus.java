package pondy.coopon.org.pprgjipmer.utils.constant;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VisitStatus {

    public static final int INACTIVE = 0;
    public static final int ACTIVE = 1;
    public static final int COMPLETED = 2;

    @IntDef({INACTIVE, ACTIVE, COMPLETED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Season {
    }
}