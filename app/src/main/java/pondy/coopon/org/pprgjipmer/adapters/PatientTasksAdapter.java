package pondy.coopon.org.pprgjipmer.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.interfaces.PatientTaskUpdateListener;
import pondy.coopon.org.pprgjipmer.interfaces.TaskUpdateListener;
import pondy.coopon.org.pprgjipmer.model.Task;
import pondy.coopon.org.pprgjipmer.model.VisitQuestion;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;

public class PatientTasksAdapter extends RecyclerView.Adapter<PatientTasksAdapter.ViewHolder> {

    private static final String TAG = "PatientTasksAdapter";

    private List<Task> questionList;
    private TaskUpdateListener mListener;
    private String tag;

    PatientTasksAdapter(List<Task> questionList, TaskUpdateListener listener, String tag) {
        this.questionList = questionList;
        this.mListener = listener;
        this.tag = tag;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        int pos = holder.getAdapterPosition();
        Task question = questionList.get(pos);
        holder.checkBoxQuestion.setText(question.getQuestion());
        holder.checkBoxQuestion.setChecked(question.getAnswer());

        holder.checkBoxQuestion.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.e("TAG", "VISIT QUESTION 1 " + tag);
            questionList.get(pos).setAnswer(isChecked);
            mListener.onTaskUpdate(tag, questionList);
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_questions, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    /* adapter view holder */
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkbox_question)
        CheckBox checkBoxQuestion;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
