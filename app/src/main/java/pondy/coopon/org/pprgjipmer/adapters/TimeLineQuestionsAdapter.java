package pondy.coopon.org.pprgjipmer.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.model.Task;
import pondy.coopon.org.pprgjipmer.model.VisitQuestion;

class TimeLineQuestionsAdapter extends RecyclerView.Adapter<TimeLineQuestionsAdapter.ViewHolder> {

    private List<Task> visitQuestionList;
    private Context mContext;

    public TimeLineQuestionsAdapter(Context context, List<Task> visitQuestionList) {
        this.mContext = context;
        this.visitQuestionList = visitQuestionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_question, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Task visitQuestion = visitQuestionList.get(position);
        holder.txtQuestion.setText(visitQuestion.getQuestion());
        holder.imgStatus.setImageDrawable(ContextCompat.getDrawable(mContext, visitQuestion.getAnswer() ? R.drawable.ic_task_completed : R.drawable.ic_task_incomplete));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.imgStatus.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, visitQuestion.getAnswer() ? R.color.task_completed : R.color.task_incomplete)));
        }
    }


    @Override
    public int getItemCount() {
        return visitQuestionList.size();
    }

    /* adapter view holder */
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_question)
        TextView txtQuestion;
        @BindView(R.id.img_status)
        ImageView imgStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}