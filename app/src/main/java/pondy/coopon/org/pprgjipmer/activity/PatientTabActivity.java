package pondy.coopon.org.pprgjipmer.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.couchbase.lite.Dictionary;
import com.couchbase.lite.Document;
import com.google.android.material.tabs.TabLayout;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.fragments.BioDataViewFragment;
import pondy.coopon.org.pprgjipmer.fragments.ContentsViewFragment;
import pondy.coopon.org.pprgjipmer.fragments.TasksFragment;
import pondy.coopon.org.pprgjipmer.fragments.VisitHistoryFragment;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.model.Mother;


public class PatientTabActivity extends AppCompatActivity {

    private static final String TAG = "PatientTabActivity";

    private DatabaseManager dbMgr;

    @BindView(R.id.main_tabs)
    TabLayout tabLayout;
    @BindView(R.id.container)
    ViewPager mViewPager;

    private String patientID;

    private Mother masterPatient = new Mother();

    private String[] titleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_tab);

        // Butter knife Init
        ButterKnife.bind(this);

        // Title List of Action bar
        titleList = new String[]{
                this.getResources().getString(R.string.patient_tab_1),
                this.getResources().getString(R.string.patient_tab_2),
                this.getResources().getString(R.string.patient_tab_3),
                this.getResources().getString(R.string.patient_tab_4)
        };


        dbMgr = DatabaseManager.getSharedInstance(this);

        Intent receiveIntent = getIntent();
        patientID = receiveIntent.getStringExtra("PATIENT_ID");

        if (patientID != null) {
            masterPatient = dbMgr.getPatient(patientID);
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        FragmentPagerAdapter mViewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mViewPageAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        setTabValues(tabLayout.getSelectedTabPosition());
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTabValues(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setTabValues(int selectedTabPosition) {
        setTitle(titleList[selectedTabPosition]);
        final int normalTab = ContextCompat.getColor(getApplicationContext(), R.color.tab_default);
        final int selectedTab = ContextCompat.getColor(getApplicationContext(), R.color.tab_selected);
        for (int i = 0; i < tabLayout.getTabCount(); i++)
            Objects.requireNonNull(Objects.requireNonNull(tabLayout.getTabAt(i)).getIcon()).setColorFilter(selectedTabPosition == i ? selectedTab : normalTab, PorterDuff.Mode.SRC_IN);
    }

    /**
     * This adapter supplies the respective fragments to be use in
     * respective tabs.
     */
    public class ViewPageAdapter extends FragmentPagerAdapter {
        private ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 1:
                    fragment = new VisitHistoryFragment(masterPatient);
                    break;
                case 2:
                    fragment = new ContentsViewFragment(masterPatient);
                    break;
                case 3:
                    fragment = new TasksFragment(masterPatient);
                    break;
                default:
                    fragment = new BioDataViewFragment(masterPatient);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return tabLayout.getTabCount();
        }
    }
}
